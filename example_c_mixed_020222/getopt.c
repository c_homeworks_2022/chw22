#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void state_machine_standart(void);
static void state_machine_direct(void);

static const char *optString = "dsh?";


int main(int argc, char **argv)
{
    int c;

    printf("argc: %d\n", argc);

    /* Decide which state machine will be used */
    while ((c = getopt(argc, argv, optString)) != -1) {
        switch (c) {
            case 'd': state_machine_direct(); break;
            case 's': state_machine_standart(); break;
            case 'h':
            case '?':
                printf("Usage: %s [%s]\n", argv[0], optString);
            break;
            default :
                /* You won't actually get here. */
            break;
        }
    }
}

static void state_machine_standart(void)
{
	printf("%s\n", __FUNCTION__);

}

static void state_machine_direct(void)
{
	printf("%s\n", __FUNCTION__);

}


