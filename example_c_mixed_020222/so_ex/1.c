#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int timerci(void) 
{ 
    int ret;
    struct timespec uptime;

    memset(&uptime, 0, sizeof(uptime));
    clock_gettime(CLOCK_MONOTONIC, &uptime);

    printf("Uptime: %ld.%02ld\n", uptime.tv_sec, uptime.tv_nsec / 10000000);
}
