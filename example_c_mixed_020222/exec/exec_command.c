#include <stdio.h>
#include <stdlib.h>

/* Execute a command */
static int exec_command(char *command, char *retval)
{
	FILE *fp;

	/* Open the command for reading. */
	fp = popen(command, "r");
	if (fp == NULL) {
		printf("Failed to run command\n");
		return -1;
	}

	/* Read the output a line at a time - output it. */
	if(retval != NULL) {
		if(fgets(retval, sizeof(retval) - 1, fp) == NULL) {
			printf("Failed to run command\n");
			return -1;
		}
	}

	pclose(fp);
}

int main( int argc, char *argv[] )
{
	char retval[10];

	exec_command("ls -al", retval);
	printf("%s\n", retval);

	return 0;
}
