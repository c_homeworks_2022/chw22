#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

static void parse(char *line, char **argv)
{
	while (*line != '\0') {
		while (*line == ' ' || *line == '\t' || *line == '\n') {
			*line++ = '\0';
		}
		*argv++ = line;
		while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') {
			line++;
		}
	}

	*argv = '\0';
}

static int execute(char *line)
{
	char  *argv[64];
	pid_t  pid;
	int    status;

	parse(line, argv);

	if ((pid = fork()) < 0) {
		return -1;
	}
	else if (pid == 0) {
		if (execvp(*argv, argv) < 0) {
			return -1;
		}
	}
	else {
		while (wait(&status) != pid);
	}
}



int  main(void)
{
     char line[1024] = "ls /projects -al";
     execute(line);
}
